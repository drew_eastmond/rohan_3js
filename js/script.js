( function () {
	"use strict";

	function AppManager () {

		var _this = this;
		var _loader;

		var _scene, _camera, _renderer, _controls;

		function _construct () {
			_loader = new THREE.GLTFLoader ();

			_scene = new THREE.Scene ();

			_camera = new THREE.PerspectiveCamera ( 75, window.innerWidth / window.innerHeight, 0.1, 1000 );
			_camera.position.set( -250, 0.9, 2.7 );

			_controls = new THREE.OrbitControls ( _camera );
			_controls.target.set ( 0, - 0.2, - 0.2 );
			_controls.update ();

			var light = new THREE.HemisphereLight ( 0xbbbbff, 0x444422 );
			light.position.set ( 0, 1, 0 );
			_scene.add ( light );

			_renderer = new THREE.WebGLRenderer ();
			_renderer.setSize ( window.innerWidth, window.innerHeight );
			document.body.appendChild ( _renderer.domElement );

		}

		_this.load = function () {
			_loader.load (
				// resource URL
				'assets/models/scene.gltf',
				// called when the resource is loaded
				function ( gltf ) {

					_scene.add ( gltf.scene );

					gltf.animations; // Array<THREE.AnimationClip>
					gltf.scene; // THREE.Scene
					gltf.scenes; // Array<THREE.Scene>
					gltf.cameras; // Array<THREE.Camera>
					gltf.asset; // Object


					_this.startScene ();
				},
				// called while loading is progressing
				function ( xhr ) {

					console.log ( ( xhr.loaded / xhr.total * 100 ) + '% loaded' );

				},
				// called when loading has errors
				function ( error ) {

					console.error ( error );

					console.log( 'An error happened' );

				}
			);
		};

		_this.startScene = function () {

			function animate () {
				requestAnimationFrame ( animate );
				_renderer.render ( _scene, _camera );
			}
			animate ();
		};

		_construct ();

	};

	var appManager = new AppManager ();
	appManager.load ();

} () );